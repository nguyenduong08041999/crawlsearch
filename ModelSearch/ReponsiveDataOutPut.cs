﻿using System;

namespace ModelSearch
{
    public class ReponsiveDataOutPut
    {


        public class Rootobject
        {
            public Class1[] Property1 { get; set; }
        }

        public class Class1
        {
            public string id { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public string columns { get; set; }
            public int totalProducts { get; set; }
            public List[] list { get; set; }
            public Displayflags displayFlags { get; set; }
            public Headercmscomponentcontainer[] headerCmsComponentContainers { get; set; }
            public Stackcmscomponentcontainer[] stackCmsComponentContainers { get; set; }
        }

        public class Displayflags
        {
            public bool displayNewSavings { get; set; }
            public bool displayNew { get; set; }
            public bool displayFamily { get; set; }
            public bool displayShortDescription { get; set; }
            public bool displayRatings { get; set; }
            public bool displayPrice { get; set; }
            public bool displayMultiPriceCallout { get; set; }
            public bool displayOfferCallout { get; set; }
            public bool displayItemLevelCallout { get; set; }
            public bool displaySwatchCount { get; set; }
            public bool displayEnsembleDescription { get; set; }
            public bool displayBadging { get; set; }
        }

        public class List
        {
            public string id { get; set; }
            public string url { get; set; }
            public string family { get; set; }
            public bool isNew { get; set; }
            public string name { get; set; }
            public float rating { get; set; }
            public int totalReviewCount { get; set; }
            public string[] productImages { get; set; }
            public object offerCallout { get; set; }
            public int swatchCount { get; set; }
            public string swatchLabel { get; set; }
            public bool isZeroAttribute { get; set; }
            public bool isNewSavings { get; set; }
            public bool isGiftCard { get; set; }
            public bool isEGiftCard { get; set; }
            public bool isClearance { get; set; }
            public string[] itemLevelCallout { get; set; }
            public string price { get; set; }
            public object altPrices { get; set; }
            public string salePrice { get; set; }
            public string collectionShortDescription { get; set; }
            public bool hideOnMobile { get; set; }
            public bool displayGenericDescription { get; set; }
            public Badge[] badges { get; set; }
        }

        public class Badge
        {
            public string type { get; set; }
            public string location { get; set; }
            public string text { get; set; }
        }

        public class Headercmscomponentcontainer
        {
            public string id { get; set; }
            public string type { get; set; }
            public string marginRight { get; set; }
            public string marginBottom { get; set; }
            public bool isRowStart { get; set; }
            public string textPosition { get; set; }
            public string theme { get; set; }
            public string textField4 { get; set; }
            public string bodyCopyColor { get; set; }
            public string textBoxWidth { get; set; }
            public string anchorName { get; set; }
        }

        public class Stackcmscomponentcontainer
        {
            public Cmscomponent cmsComponent { get; set; }
            public int position { get; set; }
            public int columnSpan { get; set; }
            public int rowSpan { get; set; }
        }

        public class Cmscomponent
        {
            public string id { get; set; }
            public string type { get; set; }
            public string width { get; set; }
            public string height { get; set; }
            public string source { get; set; }
            public string imgAltText { get; set; }
            public string marginRight { get; set; }
            public string marginBottom { get; set; }
            public bool isRowStart { get; set; }
            public string textPosition { get; set; }
            public string theme { get; set; }
            public string bodyCopyColor { get; set; }
            public string textBoxWidth { get; set; }
        }


    }
}
