﻿using Chilkat;
using HtmlAgilityPack;
using ModelSearch;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace ControlCrawlData
{
    public delegate void DataOutPutComplete(Object outdata);
    public class RequestHttp
    {
        //?brand=vs&collectionId=1369cf9c-a639-4420-9885-e41a43a9b53f&orderBy=HTL&activeCountry=US
        public static String HOST = "https://api.victoriassecret.com";
        public static String ST = "/stacks/v11/";
        public static String GetUrl(String st, String para)
        {
            return HOST + st + para;
        }
        public static String GetPara(String collectID)
        {
            return "?brand=vs&collectionId="+collectID+"&activeCountry=US";
        }
        public static void GetObject(DataOutPutComplete result)
        {
            Chilkat.Global glob = new Chilkat.Global();
            bool success = glob.UnlockBundle("apolou.cbx1020_t6ha3h6njuoe");
            if (success != true)
            {
                return;
            }
            int status = glob.UnlockStatus;
            Chilkat.Http http = new Chilkat.Http();
            string html = http.QuickGetStr(GetUrl(ST,GetPara("023565a2-910a-4273-a987-23d203c682c1")));

            List<ReponsiveDataOutPut.Class1> reponsiveJSON = JsonConvert.DeserializeObject<List<ReponsiveDataOutPut.Class1>>(html);
            List<ReponsiveDataOutPut.List> listData = new List<ReponsiveDataOutPut.List>();
            foreach(ReponsiveDataOutPut.Class1 item in reponsiveJSON)
            {
                listData.AddRange(item.list);
            }

            result(reponsiveJSON);
        }

        private static void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

            var wb = (WebBrowser)sender;
            var html = wb.Document.GetElementsByTagName("HTML")[0].OuterHtml;

        }
    }
}
//HtmlWeb web = new HtmlWeb();
//var htmlDoc = web.Load("https://www.victoriassecret.com/us/vs/sale/clearance-bras?scroll=true&filter=size%3ASmall-DD%7CM%7CL%7CXL%7C30B%7C30C%7C30D%7C32A%7C32B%7C32C%7C32D%7C32DD%20(E)%7C32DDD%20(F)%7C34A%7C34B%7C34C%7C34D%7C34DD%20(E)%7C34DDD%20(F)%7C2&orderBy=REC");;
//var node = htmlDoc.DocumentNode.SelectSingleNode("/html/body/div/main/div/div/ul/li/article/a");
//String a = node.InnerText;